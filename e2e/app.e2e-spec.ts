import { AppNewAlphavillePage } from './app.po';

describe('app-new-alphaville App', function() {
  let page: AppNewAlphavillePage;

  beforeEach(() => {
    page = new AppNewAlphavillePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
