import * as moment from 'moment';
export class TelemetryUtil {
    public static formatToChart(dataList: Array<any>) {
        const chartData = [];
        const chartTs = [];
        console.log('dataList', dataList);
        // tslint:disable-next-line:forin
        for (const i in dataList) {
            const dataValue = dataList[i];
            chartData[i] = parseFloat(dataValue.value).toFixed(2);
            // tslint:disable-next-line:radix
            console.log('dataValue.ts', dataValue.ts);
            chartTs[i] = moment(dataValue.ts).format('DD/MM/YY HH:MM');
            // tslint:disable-next-line:radix
            // chartTs[i] = new Date(parseInt(dataValue.ts)).toString();

        }
        console.log('chartTs', chartTs);
        return [chartData.reverse(), chartTs.reverse()];
    }

    public static countInternetChart(dataList: Array<any>) {
        const chartInternet = [];
        let countOffline = 0;
        let countOnline = 0;

        // tslint:disable-next-line:forin
        for (const i in dataList) {
            const dataValue = dataList[i];
            chartInternet[i] = dataValue.value;
            if (dataValue.value === 'false') {
                countOffline = countOffline + 1;
            } else if (dataValue.value === 'true') {
                countOnline = countOnline + 1;
            }
        }


        return [countOffline, countOnline];
    }
    public static countSewageLevelChart(dataList: Array<any>) {
        const chartSewageLevel = [];
        let countOverflow = 0;
        let countNormal = 0;

        // tslint:disable-next-line:forin
        for (const i in dataList) {
            const dataValue = dataList[i];
            chartSewageLevel[i] = dataValue.value;
            if (dataValue.value >= 100) {
                countOverflow = countOverflow + 1;
            } else if (dataValue.value < 100) {
                countNormal = countNormal + 1;
            }
        }
        return [chartSewageLevel, countOverflow, countNormal];
    }


    public static formatToChartInteger(dataList: Array<any>) {
        const chartData = [];
        // tslint:disable-next-line:forin
        for (const i in dataList) {
            const dataValue = dataList[i];
            chartData.push({
                // tslint:disable-next-line:radix
                value: parseInt(dataValue.value).toString(),
                // tslint:disable-next-line:radix
                name: new Date(parseInt(dataValue.ts)).toString()
            });
        }
        return chartData;
    }
    // function to sum all values coming from TB's API where interval is one day
    
}
