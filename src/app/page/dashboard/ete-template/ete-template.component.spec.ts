import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EteTemplateComponent } from './ete-template.component';

describe('EteTemplateComponent', () => {
  let component: EteTemplateComponent;
  let fixture: ComponentFixture<EteTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EteTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EteTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
