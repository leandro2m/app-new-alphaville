import { Component, OnInit, Input } from '@angular/core';
import { TelemetryService } from '../../../services/telemetry.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ete-template',
  templateUrl: './ete-template.component.html',
  styleUrls: ['./ete-template.component.scss']
})
export class EteTemplateComponent implements OnInit {

  public volumeValue: number ;
  private deviceId: string;

  // tslint:disable-next-line:no-input-rename
  @Input('id')
  set id(value: string) {
    this.deviceId = value;
  }

  // tslint:disable-next-line:ban-types
  // tslint:disable-next-line:ban-types
  data: Object;
  public alertWaterTank = '';
  public cssTankAlert = '';
  internetstatus = '';
  aB1F1: any;
  aB2F1: any;
  sewageLevel: number;
  vB1F1: number;
  vB1F2: number;
  vB1F3: number;
  vB2F1: number;
  vB2F2: number;
  vB2F3: number;
  public deviceName: string;

  gaugeType = "semi";
  currentLabel = "Corrente";
  phaseLabel1 = "Fase 1"
  phaseLabel2 = "Fase 2"
  phaseLabel3 = "Fase 3"
  currentAppendText = "amp";
  phaseAppendText = "V"

  gaugeForegroundColor = '#03FFA9';



  constructor(
    private telemetryService: TelemetryService,
    private router: Router
    ) {

  }

  ngOnInit() {
    this.getLastTelemetry();
    this.getDeviceInfos();
  }

  getLastTelemetry() {
    this.telemetryService
    .getDeviceLastData(this.deviceId, ['internetstatus,aB1F1,aB2F1,sewageLevel,vB1F1,vB1F2,vB1F3,vB2F1,vB2F2,vB2F3']).then(response => {

      this.internetstatus = response['internetstatus'][0].value;
      // tslint:disable-next-line:no-string-literal
      this.aB1F1 = response['aB1F1'][0].value;
      this.aB2F1 = response['aB2F1'][0].value;
      this.sewageLevel = response['sewageLevel'][0].value;
      this.vB1F1 = response['vB1F1'][0].value;
      this.vB1F2 = response['vB1F2'][0].value;
      this.vB1F3 = response['vB1F3'][0].value;
      this.vB2F1 = response['vB2F1'][0].value;
      this.vB2F2 = response['vB2F2'][0].value;
      this.vB2F3 = response['vB2F3'][0].value;

      this.data = {
          'chart': {
          "theme": "fusion",
          'subcaptionFontBold': '0',
          'lowerLimit': '0',
          'upperLimit': '100',
          'numberSuffix': '',
          'showValue': '0',
          'cylFillColor': '#01ACFF',
          'borderAlpha': '0',
          'showTickMarks': '0',
          'showTickValues': '0',
          "bgColor":"#3D3780",
          "bgratio": "0",
          "bgalpha": "100,0",
          "showBorder": "0",
          "baseFontColor": "#ffffff",
          "showToolTip": "0",
          'lowerLimitDisplay': 'Vazio',
          'upperLimitDisplay': 'Cheio'
        },
        'value': this.sewageLevel
      };


    });

  }

  getDeviceInfos() {
    this.telemetryService.getDeviceInfos(this.deviceId).then(response => {
      this.deviceName = response['additionalInfo'].description;
      console.log('name', this.deviceName);

    });
  }

  goToDeviceDetail() {
    this.router.navigateByUrl('sensor/' + this.deviceId);
  }

}
