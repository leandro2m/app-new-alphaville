import { Component, OnInit } from '@angular/core';
import { eteAdata, eteBdata, eteCdata } from '../../../environments/environment';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  eteAdata = eteAdata.deviceId;
  eteBdata = eteBdata.deviceId;
  eteCdata = eteCdata.deviceId;


  ngOnInit() {
  }

}
