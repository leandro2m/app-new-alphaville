import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { FusionChartsModule } from 'angular4-fusioncharts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';


// Load FusionCharts
import * as FusionCharts from 'fusioncharts';
// Load Charts module
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as Widgets from 'fusioncharts/fusioncharts.widgets';
import * as FintTheme from 'fusioncharts/themes/fusioncharts.theme.fint';
import { SensorDetailComponent } from './sensor-detail/sensor-detail.component';
import { SolarComponent } from './solar/solar.component';
import { DeviceTemplateComponent } from './device-template/device-template.component';
import { NgxGaugeModule } from 'ngx-gauge';
import { EteTemplateComponent } from './ete-template/ete-template.component';

// Load fusion theme

FusionChartsModule.fcRoot(FusionCharts, Widgets, Charts, FintTheme);

@NgModule({
  declarations: [
    DashboardComponent,
    SolarComponent,
    SensorDetailComponent,
    DeviceTemplateComponent,
    EteTemplateComponent,
    ],
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    FusionChartsModule,
    BrowserModule,
    NgxChartsModule,
    NgxEchartsModule,
    ThemeModule,
    NgxGaugeModule
  ],
  exports: [
    DashboardComponent,

  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class DashboardModule { }
