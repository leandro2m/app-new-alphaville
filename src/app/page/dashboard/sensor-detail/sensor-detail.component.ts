import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TelemetryService } from 'src/app/services/telemetry.service';
import * as moment from 'moment';
import { TelemetryUtil } from '../../../services/util/telemetry-util';





@Component({
  selector: 'app-sensor-detail',
  templateUrl: './sensor-detail.component.html',
  styleUrls: ['./sensor-detail.component.scss']
})
export class SensorDetailComponent implements OnInit {

  public id: string;
  sewageLevel: number;
  sysTemp: number;
  sysHumi: number;
  deviceName: string;
  internetstatus: boolean;
  sewageLevelChart = [];
  chartETEA = [];
  internetChart = [];
  countWaterlevel = [];
  endDate = new Date();
  startDate = moment(this.endDate).subtract(7, 'days').toDate().getTime();
  marks = 345600000;
  options: any = {};
  testeOption: any = {};
  chartInternetOptions: any = {};
  chartWaterlevelOptions: any = {};
  internetResilience = 0.0;
  reservMsg = '';
  reservMsgCss = '';
  types = ['Diario', 'Semanal', 'Mensal'];
  // tslint:disable-next-line:variable-name
  selected_period = 'Semanal';
  timeOfLastPost: number;

  @Output() public change: EventEmitter<any> = new EventEmitter();

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private telemetryService: TelemetryService,
              ) {

              }


  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
      if (!this.id) {
        this.router.navigateByUrl('dashboard');
      } else {
        this.initPage();
        this.getDeviceInfos();
        this.updateChartSewageLevel(this.startDate, this.marks);
        this.updateChartInternet(this.startDate, this.marks);
        this.updateChartCountSewageLevelPieChart(this.startDate, this.marks);

      }
    });
  }

  initPage() {

    this.telemetryService
    .getDeviceLastData(this.id, ['sewageLevel,internetstatus']).then(response => {

      this.timeOfLastPost = response['internetstatus'][0].ts;


      this.sewageLevel = response['sewageLevel'][0].value;
      // tslint:disable-next-line:radix
      this.internetstatus = response['internetstatus'][0].value;
      // tslint:disable-next-line:radix

      if (this.sewageLevel >= 100) {
        this.reservMsg = 'Sensor no nível de transbordo';
        this.reservMsgCss = 'badge-danger';
      } else {
        this.reservMsg = 'Sensor nível normal';
        this.reservMsgCss = 'badge-success';
      }
  });
  }

  getDeviceInfos() {
    this.telemetryService.getDeviceInfos(this.id).then(response => {
      this.deviceName = response['additionalInfo'].description;

    });
  }


  updateChartSewageLevel(startdate, marks) {

    const startTimeStamp = startdate;
    const endTimeStamp = moment(this.endDate).toDate().getTime();
    const interval = marks;


    this.telemetryService.getDeviceTimeSeries(this.id, ['sewageLevel'],
      startTimeStamp, endTimeStamp, interval).then(response => {

      console.log('response sewageLevel', response);
      this.sewageLevelChart = TelemetryUtil.formatToChart(response['sewageLevel']);

      this.options = {
        backgroundColor: 'rgb(49, 45, 112)',
        color: ['rgb(51, 159, 255)'],
        tooltip: {
          show: true,
          trigger: 'axis',
          axisPointer: {
            type: 'cross',
            label: {
              backgroundColor: 'rgb(61, 55, 128)',
            },
          },
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true,
        },
        xAxis: [
          {
            show: true,
            type: 'category',
            boundaryGap: false,
            data: this.sewageLevelChart[1],
            axisTick: {
              alignWithLabel: true,
            },
            axisLine: {
              lineStyle: {
                color: 'rgba(255, 255, 255, 255)',
              },
            },
            axisLabel: {
              textStyle: {
                color: 'rgba(255, 255, 255, 255)',
              },
            },
          },
        ],
        yAxis: [
          {
            type: 'value',
            axisLine: {
              lineStyle: {
                color: 'rgba(255, 255, 255, 255)',
              },
            },
            splitLine: {
              lineStyle: {
                color: 'rgba(255, 255, 255, 255)',
              },
            },
            axisLabel: {
              textStyle: {
                color: 'rgba(255, 255, 255, 255)',
              },
            },
          },
        ],
        series: [
          {
            name: 'Reservatorio',
            type: 'line',
            stack: 'Total amount',
            areaStyle: {normal: {} },
            data: this.sewageLevelChart[0],
          },

        ],
      };
      });
  }

  formatChartValue(value) {
    return `${value.toLocaleString()}` + ' %';
  }

  formatChartDateTimeValue(value) {
    const dateObj = new Date(value);
    return moment(dateObj).format('DD/MM/YYYY HH:mm');
  }

  updateChartInternet(startdate, marks) {

    const startTimeStamp = startdate;
    const endTimeStamp = moment(this.endDate).toDate().getTime();
    const interval = marks;


    this.telemetryService.getDeviceTimeSeries(this.id, ['internetstatus'],
      startTimeStamp, endTimeStamp, interval).then(response => {

      // this.internetChart = TelemetryUtil.countInternetChart(response['internetstatus']);
      this.internetChart = TelemetryUtil.countInternetChart(response['internetstatus']);
      console.log('internetChart', this.internetChart);

      this.internetResilience = this.internetChart[1] / this.internetChart[0].length;

      this.chartInternetOptions = {
        backgroundColor: 'rgb(49, 45, 112)',
        color: ['rgb(51, 224, 146)', 'rgb(255, 61, 116)'],
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {d}%',
        },
        legend: {
          orient: 'vertical',
          left: 'left',
          data: ['Online', 'Offline'],
          textStyle: {
            color: 'rgba(255, 255, 255, 255)',
          },
        },
        series: [
          {
            name: 'Conectividade',
            type: 'pie',
            radius: '80%',
            center: ['50%', '50%'],
            data: [
              { value: this.internetChart[1], name: 'Online' },
              { value: this.internetChart[0], name: 'Offline' },
            ],
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(255, 255, 255, 255)',
              },
            },
            label: {
              normal: {
                textStyle: {
                  color: 'rgba(255, 255, 255, 255)',
                },
              },
            },
            labelLine: {
              normal: {
                lineStyle: {
                  color: 'rgba(0, 0, 0, 0)',
                },
              },
            },
          },
        ],
      };
    });
  }

  updateChartCountSewageLevelPieChart(startdate, marks) {

    const startTimeStamp = startdate;
    const endTimeStamp = moment(this.endDate).toDate().getTime();
    const interval = marks;

    this.telemetryService.getDeviceTimeSeries(this.id, ['sewageLevel'],
      startTimeStamp, endTimeStamp, interval).then(response => {

      // pie chart
      this.countWaterlevel = TelemetryUtil.countSewageLevelChart(response['sewageLevel']);
      console.log('countwaterlevel', this.countWaterlevel);

      this.chartWaterlevelOptions = {
        backgroundColor: 'rgb(49, 45, 112)',
        color: ['rgb(51, 224, 146)', 'rgb(255, 61, 116)', 'rgb(255, 196, 56)'],
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {d}%',
        },
        legend: {
          orient: 'vertical',
          left: 'left',
          data: ['Cheio', 'Transbordo'],
          textStyle: {
            color: 'rgba(255, 255, 255, 255)',
          },
        },
        series: [
          {
            name: 'Volume',
            type: 'pie',
            radius: '80%',
            center: ['50%', '50%'],
            data: [
              // tslint:disable-next-line:radix
              { value: this.countWaterlevel[2] , name: 'Cheio' },
              { value: this.countWaterlevel[1] , name: 'Transbordo' },
            ],
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(255, 255, 255, 255)',
              },
            },
            label: {
              normal: {
                textStyle: {
                  color: 'rgba(255, 255, 255, 255)',
                },
              },
            },
            labelLine: {
              normal: {
                lineStyle: {
                  color: 'rgba(0, 0, 0, 0)',
                },
              },
            },
          },
        ],
      };

      });



  }
  onChangePeriod(start) {

    const newPeriod = start;
    // tslint:disable-next-line:no-unused-expression
    let newStartDate: number;
    let newMark: number;

    if (newPeriod === 'Diario') {
      newStartDate = moment(this.endDate).subtract(1, 'days').toDate().getTime();
      newMark = 86400000;
      this.selected_period = 'Diario';

      this.updateChartSewageLevel(newStartDate, newMark);
      this.updateChartInternet(newStartDate, newMark);
      this.updateChartCountSewageLevelPieChart(newStartDate, newMark);


    } else if (newPeriod === 'Semanal') {
      newStartDate = moment(this.endDate).subtract(7, 'days').toDate().getTime();
      newMark = 345600000; // 4 hours interval
      this.selected_period = 'Semanal';
      this.updateChartSewageLevel(newStartDate, newMark);
      this.updateChartInternet(newStartDate, newMark);
      this.updateChartCountSewageLevelPieChart(newStartDate, newMark);

    } else if (newPeriod === 'Mensal') {
      newStartDate = moment(this.endDate).subtract(30, 'days').toDate().getTime();
      newMark = 691200000; // 8 hours interval
      this.selected_period = 'Mensal';
      this.updateChartSewageLevel(newStartDate, newMark);
      this.updateChartInternet(newStartDate, newMark);
      this.updateChartCountSewageLevelPieChart(newStartDate, newMark);

    }

  }

  goBack() {
    this.router.navigateByUrl('dashboard');
  }



}
