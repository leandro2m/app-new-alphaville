import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './page/home-page/home-page.component';
import { LoginPageComponent } from './page/login-page/login-page.component';
import { DashboardComponent } from './page/dashboard/dashboard.component';
import { SensorDetailComponent } from './page/dashboard/sensor-detail/sensor-detail.component';
import { LogoutPageComponent } from './page/logout-page/logout-page.component';
import { AnalyticsPageComponent } from './page/analytics-page/analytics-page.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomePageComponent
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'logout',
    component: LogoutPageComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'sensor/:id',
    component: SensorDetailComponent
  },
  {
    path: 'analytics',
    component: AnalyticsPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
