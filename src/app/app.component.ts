import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { NbSidebarService, NbMenuItem, NbMenuService, NbThemeService,
   NbMediaBreakpoint, NbMediaBreakpointsService } from '@nebular/theme';
import { delay, withLatestFrom, takeWhile } from 'rxjs/operators';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {


  public title = 'Condomínio Alphaville';
  private alive = true;
  subMenu: NbMenuItem[] = [
    {
      title: 'MENU',
      group: true,
    },
    {
      title: 'DASHBOARD',
      icon: 'fas fa-home',
      link: '/dashboard'
    },
    {
      title: 'ANALYTICS',
      icon: 'fas fa-chart-area',
      link: '/analytics'
    },
    {
      title: 'SAIR',
      icon: 'fas fa-sign-out-alt',
      link: '/logout'
    }
   ];

  constructor(
    private router: Router,
    public authService: AuthService,
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private bpService: NbMediaBreakpointsService,
  ) {

    const isBp = this.bpService.getByName('is');

    this.menuService.onItemSelect()
      .pipe(
        takeWhile(() => this.alive),
        withLatestFrom(this.themeService.onMediaQueryChange()),
        delay(20),
      )
      .subscribe(([item, [bpFrom, bpTo]]: [any, [NbMediaBreakpoint, NbMediaBreakpoint]]) => {

        if (bpTo.width <= isBp.width) {
          this.sidebarService.collapse('menu-sidebar');
        }
      });
    this.sidebarService.onCollapse();
  }

  ngOnInit() {
    if (!this.authService.checkAuth()) {
      this.router.navigateByUrl('login');
    }

  }
  goAllu() {
    this.router.navigateByUrl('dashboard');
  }
  toggle() {
    this.sidebarService.toggle(false);
    return true;
  }

}
